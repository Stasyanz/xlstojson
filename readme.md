# XLS to JSON

Convert your xls workbook into json file

## Using

`$ python app.py -i=data_test.xlsm -o=your_json_file.json`

options

-i --input Input .xlsm file



- if `-o` is not provided, .json file will be saved in the current directry

- if output file already exists, it will not be overwritten

- if output file is not provided, it takes the name of the input file


Use data_test.xlsm to test as it has less rows than data.xlsm
