import sys
import openpyxl
import json
import datetime
from argparse import ArgumentParser
from pathlib import Path
import time


class XLSTOJSON:
    """xls to json converter"""
    def __init__(self, input_path, output_path):
        self.input_path = input_path
        self.output_path = output_path

    def load_workbook(self):
        print("Loading file..")
        t0 = time.time()
        wb = openpyxl.load_workbook(self.input_path)
        t1 = time.time() - t0
        print("File loaded in {} seconds".format(t1))
        return wb

    @staticmethod
    def get_worksheets(wb):
        sheets = wb.worksheets
        return sheets

    @staticmethod
    def get_header(row):
        header = list()
        for item in row:  # get first line data (header)
            if item.value:
                header.append(item.value)
        return header

    def get_sheet_data(self, sheet):
        t = tuple(sheet.rows)
        sheet_title = sheet.title
        header = self.get_header(t[0])
        obj = dict()  # result sheet data
        l = list()  # temporary object to keep row data
        for i in range(sheet.max_row):
            print("Row {}".format(i))
            row = sheet[i+1]
            d = dict()
            for j in range(0, sheet.max_column):
                if isinstance(row[j].value, datetime.datetime):
                    d[header[j]] = row[j].value.strftime('%Y-%m-%dT%H:%M:%S.%f')[:-3] + 'Z'
                elif isinstance(row[j].value, datetime.time):
                    d[header[j]] = str(row[j].value)
                elif isinstance(row[j].value, str):
                    if all([row[j].value.startswith("{"), row[j].value.endswith("}")]):
                        d[header[j]] = json.loads(row[j].value)
                else:
                    d[header[j]] = row[j].value
            l.append(d)
        l = l[1:]  # exclude first ros as it is a header
        obj[sheet_title] = l
        return obj

    def save_data(self, data):
        with open(self.output_path, "w") as f:
            f.write(json.dumps(data, indent=4, ensure_ascii=False))

    def process(self):
        wb = self.load_workbook()
        sheets = self.get_worksheets(wb)
        result = dict()
        for sheet in sheets:
            sheet_data = self.get_sheet_data(sheet)
            result.update(sheet_data)
        self.save_data(result)


def get_arguments():
    """get command line arguments"""
    parser = ArgumentParser()
    parser.add_argument("-i", "--input",
                        default="data_test.xlsm",
                        help="input file path")
    parser.add_argument("-o", "--output",
                        default="",
                        help="output file path")
    args = parser.parse_args()
    return args


def main():
    args = get_arguments()
    input_path = args.input
    # Check input file
    if input_path == "":
        print("No input file provided. Exiting")
        sys.exit(1)
    if not Path(input_path).exists():
        print("Input file does not exist")
        sys.exit(1)
    output_path = args.output
    # Check output file
    if output_path == "":
        output_path = input_path.split('.')[0] + '.json'
    output_file = Path(output_path)
    if output_file.exists():
        print("Output file already exists")
        sys.exit(1)
    else:
        print("Starting job..")
        t0 = time.time()
        xls_processor = XLSTOJSON(input_path=input_path, output_path=output_path)
        xls_processor.process()
        t1 = time.time() - t0
        print("Job done. Took {} seconds".format(t1))


if __name__ == '__main__':
    main()
